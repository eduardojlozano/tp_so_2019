#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <ulimit.h>
#include <ulimit.h>

#define _XOPEN_SOURCE
#define	RLIMIT_NPROC	7
#define	RLIMIT_FSIZE	1	

int main(){
	int x = 0;
	int y;
	//struct rlimit limit;
	//limit.rlim_cur = 10;
  	//limit.rlim_max = 50;
  	//setrlimit(RLIMIT_FSIZE, &limit);
  	//setrlimit(5);
	//printf("%i\n", UL_SETFSIZE);
	//y = ulimit(UL_GETFSIZE);
	//printf("%i\n", y);
	//printf("%i\n", UL_GETFSIZE);
	
	while(1){
		int var = fork();
		ulimit('ulimit -u 10');
		if(var < 0){
			printf("Error!!");
		}
		else if(var > 0){
			printf("Soy el proceso PADRE, PID %i\n", var);
		}
		else{
			printf("Soy el proceso HIJO, PID: %i\n", var);
		}	
		
	}
	return 0;
	
}
