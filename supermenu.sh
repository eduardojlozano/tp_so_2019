#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
    red=`tput setaf 1`;
    green=`tput setaf 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
#------------------------------------------------------
# VARIABLES GLOBALES
#------------------------------------------------------
#------------
proyectoActual="/home/sor1";
proyectos="/home/sor1/Documents/repo_GitLab/repos.txt";
proceso="";
path="";

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
    
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver contenido de carpeta";
    echo -e "\t\t\t b.  Ver info de PCB";
    echo -e "\t\t\t c.  Ver PID de proceso buscado";
    echo -e "\t\t\t d.  Guardar PID";        
    echo -e "\t\t\t e.  Mostrar contenido de archivo creado";
    echo -e "\t\t\t f.  Buscar y terminar Proceso";
    echo -e "";
    echo -e "\t\t\t Git Managaer";      
    echo -e "\t\t\t g.  Ver estado del Repositorio Git";
    echo -e "\t\t\t h.  Pull desde repositorio Git";
    echo -e "\t\t\t i.  Hacer commit a local Git";
    echo -e "\t\t\t j.  Actualizar repositorio remoto Git";   
    echo -e "\t\t\t q.  Salir";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir(){
    echo $1;
    while true; do
	echo "${red}desea ejecutar? (s/n)${reset}";
    	     read respuesta;
    	     case $respuesta in
                 [Nn]* ) break;;
                    [Ss]* ) eval $1     	
    	         break;;
		 * ) echo "Por favor tipear S/s ó N/n.";;
	     esac
    done

}

contenido () {
    decidir "ls -l";
}

ver_info_de_proceso(){
    echo "${red}PID Supermenu${reset}";
    ps aux | grep supermenu.sh;
    sleep 1;
    echo -e "\t\t";
    ls /proc/;
    echo "${blue}Ingresar pid${reset}";
    read pid;
    decidir "cd /proc/$pid/ && 
    sed '1!d' status && 
    sed '3!d' status &&
    sed '6!d' status &&
    sed '7!d' status &&
    tail status -n 2 &&
    cd ${proyectoActual}";
}

buscar_proceso(){
	echo "${blue}Ingresar nombre de proceso a buscar${reset}";
    read process;
    proceso=$process
    decidir "ps aux | grep $process;"
}

guardar_proceso(){
	if [ ! -z "$proceso" ];
	then
		path="$(eval pwd)/${proceso}.txt";
		echo "Se Almacenará proceso en: ${path}"
		commands="ps aux | grep ${proceso} > ${proceso}.txt";
		decidir "${commands}";
		proceso="";
	else
		echo "No hay proceso para almacenar. Realice paso \"c\" y vuelva a intentarlo";
	fi
}

listar_archivo(){
	echo "${path}";
	if [ -f "${path}" ];
	then
		decidir "cat ${path}";
		path="";
	else
		echo "No se ha creado ningun archivo para listar";
	fi
}

terminar_proceso(){
	echo "${blue}Ingresar nombre de proceso a terminar${reset}";
	read process;
	proceso=$process;
	pid="$(eval pidof -s ${proceso})";
	decidir "kill $pid";
}

ver_estado(){
    decidir "git status";
}

actualizar_proyecto(){
    decidir "git pull";
}

commit(){
    decidir "git add .";
    sleep 2
    echo "${red}Escriba identificador del commit:${reset}";
	   read respuesta;
    union="git commit -m \"${respuesta}\"";
    decidir "${union}";
}

push(){
    decidir "git push"   
}


#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
    imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
    contenido;
}

b_funcion () {
   	imprimir_encabezado "\tOpción b.  Ver información de proceso";
    ver_info_de_proceso;
}

c_funcion () {
    imprimir_encabezado "\tOpción c.  Ver PID de proceso";
	buscar_proceso;       
}

d_funcion () {
  	imprimir_encabezado "\tOpción d.  Almacenar PID de proceso en archivo";
    guardar_proceso;
}

e_funcion () {
	imprimir_encabezado "\tOpción e  Listar informacion de archivo";        
	listar_archivo;
}

f_funcion () {
    imprimir_encabezado "\tOpción f  Buscar y terminar proceso";
    terminar_proceso;
}

g_funcion () {
    imprimir_encabezado "\tOpción g. Ver estado del repositorio Git";
    ver_estado;
}

h_funcion () {
    imprimir_encabezado "\tOpción g. Pull a repositorio Git";
    actualizar_proyecto;
}

i_funcion () {
    imprimir_encabezado "\tOpción g. Commit a repositorio local Git";
    commit;
}

j_funcion () {
    imprimir_encabezado "\tOpción g. Push a repositorio remoto Git";
    push;
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        g|G) g_funcion;;
        h|H) h_funcion;;
        i|I) i_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done
 

